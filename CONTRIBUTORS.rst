Contributors
============

Core Developers
----------------

These contributors have commit flags for the repository,
and are able to accept and merge pull requests.

=========================== ============= ================
Name                        Gitlab        Twitter
=========================== ============= ================
Scott Sharkey               `@lanshark`_  @lansharkconsult
=========================== ============= ================

Other Contributors
-------------------

Listed in alphabetical order.

=========================== ============= ============
Name                        Gitlab        Twitter
=========================== ============= ============
Your Name Here...
=========================== ============= ============

Special Thanks
~~~~~~~~~~~~~~

While the following were not directly involved in this project, they are a huge part
of the inspiration.

`pydanny`_
`audreyr`_.

.. _audreyr: https://github.com/audreyr/
.. _pydanny: https://github.com/pydanny
.. _@lanshark: https://gitlab.com/lanshark82
